package com.kpi.prks.data;

/**
 * Created by insane on 15.09.14.
 */

import java.util.Arrays;
import java.util.StringJoiner;
import java.util.StringTokenizer;

/**
 * adjacency matrix
 */

public class Graf {

    private final boolean[][] matrix;

    public Graf(boolean[][] matrix) {
        this.matrix = matrix;
    }

    public boolean[][] getMatrix() {
        return matrix;
    }

    public int vertexCount() {
        return matrix.length;
    }

    @Override
    public String toString() {
        final StringJoiner result = new StringJoiner("\n");
        for (final boolean[] vector : matrix) {
            final StringJoiner lineJoiner = new StringJoiner(",");
            for (final boolean value : vector) {
                lineJoiner.add(value ? "1" : "0");
            }
            result.add(lineJoiner.toString());
        }
        return result.toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof Graf)) {
            return false;
        }
        final Graf other = (Graf) obj;
        if (other.vertexCount() != this.vertexCount()) {
            return false;
        }
        for (int indexH = 0; indexH < this.vertexCount(); indexH++) {
            if (!Arrays.equals(matrix[indexH], other.getMatrix()[indexH])) {
                return false;
            }
        }
        return true;
    }

    public static Graf valueOf(String str) {
        final StringTokenizer valueTokenizer = new StringTokenizer(str, "\n");
        final int h = valueTokenizer.countTokens();
        final boolean[][] matrix = new boolean[h][];
        int hIndex = 0;
        for (; valueTokenizer.hasMoreTokens(); ) {
            final String line = valueTokenizer.nextToken();
            final StringTokenizer lineTokenizer = new StringTokenizer(line, ",");
            final int curW = lineTokenizer.countTokens();
            if (curW != h) {
                throw new IllegalArgumentException("Line " + hIndex + ") \"" + line + "\": has wrong width '" + curW + "~" + h + "'");
            }
            matrix[hIndex] = new boolean[curW];
            int wIndex = 0;
            for (; lineTokenizer.hasMoreTokens(); ) {
                final String val = lineTokenizer.nextToken();
                switch (val) {
                    case "0":
                        matrix[hIndex][wIndex++] = false;
                        break;
                    case "1":
                        matrix[hIndex][wIndex++] = true;
                        break;
                    default:
                        throw new IllegalArgumentException("Value " + wIndex + ") \"" + val + "\" is wrong");
                }
            }
            hIndex++;
        }
        return new Graf(matrix);
    }

}
