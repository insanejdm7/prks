package com.kpi.prks.util;

import com.kpi.prks.build.Builder;
import com.kpi.prks.data.Graf;
import com.kpi.prks.dijkstra.EdgeDijkstra;
import com.kpi.prks.dijkstra.GraphDijkstra;
import com.kpi.prks.dijkstra.VertexDijkstra;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by insane on 17.09.14.
 */
public class GraphDijkstraBuilder implements Builder<GraphDijkstra> {

    private Graf graf;

    public GraphDijkstraBuilder(Graf graf) {
        this.graf = graf;
    }

    @Override
    public GraphDijkstra build() {
        final List<VertexDijkstra> vertexes = buildVertexes();
        final List<EdgeDijkstra> edges = buildEdges(vertexes);
        return new GraphDijkstra(vertexes,edges);
    }

    private List<VertexDijkstra> buildVertexes() {
        final List<VertexDijkstra> result = new ArrayList<>(graf.vertexCount());
        for (int index = 0; index < graf.vertexCount(); index++) {
            result.add(new VertexDijkstra(index));
        }
        return result;
    }

    private List<EdgeDijkstra> buildEdges(List<VertexDijkstra> vertexes) {
        final List<EdgeDijkstra> result = new ArrayList<>();
        for (int indexH = 0; indexH < graf.vertexCount(); indexH++) {
            for (int indexW = 0; indexW < graf.vertexCount(); indexW++) {
                if (graf.getMatrix()[indexH][indexW]) {
                    result.add(new EdgeDijkstra(vertexes.get(indexH), vertexes.get(indexW), 1));
                }
            }
        }
        return result;
    }
}
