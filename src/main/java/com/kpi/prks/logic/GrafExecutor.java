package com.kpi.prks.logic;

import com.kpi.prks.data.Graf;
import com.kpi.prks.dijkstra.DijkstraAlgorithm;
import com.kpi.prks.dijkstra.GraphDijkstra;
import com.kpi.prks.util.GraphDijkstraBuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by insane on 17.09.14.
 */
public class GrafExecutor {

    private Graf graf;
    private GraphDijkstra graphDijkstra;

    private int s;
    private int[][] minMatrix;
    private int d;
    private double avgD;
    private double t;
    private int c;

    public GrafExecutor(Graf graf) {
        this.graf = graf;
        graphDijkstra = new GraphDijkstraBuilder(graf).build();
    }

    public int calcS() {
        return s;
    }

    private int performS() {
        int max = 0;
        for (boolean[] arr : graf.getMatrix()) {
            int tmp = 0;
            for (boolean value : arr) {
                tmp += value ? 1 : 0;
            }
            max = tmp > max ? tmp : max;
        }
        return max;
    }

    public int[][] calcMinMatrix() {
        return minMatrix;
    }

    public GrafExecutor execute() {
        s = performS();
        minMatrix = performMinMatrix();
        d = performD();
        avgD = performAvgD();
        t = performT();
        c = performC();
        return this;
    }

    private int[][] performMinMatrix() {
        final int[][] result = new int[graf.vertexCount()][graf.vertexCount()];
        final DijkstraAlgorithm algorithm = new DijkstraAlgorithm(graphDijkstra);
        for (int indexH = 0; indexH < graf.vertexCount(); indexH++) {
            algorithm.execute(graphDijkstra.getVertexes().get(indexH));
            for (int indexW = 0; indexW < graf.vertexCount(); indexW++) {
                if (indexH != indexW) {
                    result[indexH][indexW] = algorithm.getPath(graphDijkstra.getVertexes().get(indexW)).size()-1;
                }
            }
        }
        return result;
    }

    public int calcD() {
        return d;
    }

    private int performD() {
        final int[][] matrix = calcMinMatrix();
        final List<Integer> maxes = new ArrayList<>(matrix.length);
        for (int[] arr : matrix) {
            maxes.add(Arrays.stream(arr).max().getAsInt());
        }
        return maxes.stream().max(Integer::compare).get();
    }

    public double calcAvgD() {
        return avgD;
    }

    private double performAvgD() {
        final int[][] matrix = calcMinMatrix();
        double sum = 0;
        for (int[] arr : matrix) {
            sum += Arrays.stream(arr).sum();
        }
        return sum/(matrix.length*(matrix.length-1));
    }

    public double calcT() {
        return t;
    }

    private double performT() {
        return 2*calcAvgD()/calcS();
    }

    public int calcC() {
        return c;
    }

    private int performC() {
        return calcD()*calcS()*graf.vertexCount();
    }

}
