package com.kpi.prks.dijkstra;

import java.util.*;

/**
 * Created by insane on 16.09.14.
 */
public class DijkstraAlgorithm {
    private final List<VertexDijkstra> nodes;
    private final List<EdgeDijkstra> edges;
    private Set<VertexDijkstra> settledNodes;
    private Set<VertexDijkstra> unSettledNodes;
    private Map<VertexDijkstra, VertexDijkstra> predecessors;
    private Map<VertexDijkstra, Integer> distance;

    public DijkstraAlgorithm(GraphDijkstra graph) {
        // create a copy of the array so that we can operate on this array
        this.nodes = new ArrayList<>(graph.getVertexes());
        this.edges = new ArrayList<>(graph.getEdges());
    }

    public void execute(VertexDijkstra source) {
        settledNodes = new HashSet<>();
        unSettledNodes = new HashSet<>();
        distance = new HashMap<>();
        predecessors = new HashMap<>();
        distance.put(source, 0);
        unSettledNodes.add(source);
        while (unSettledNodes.size() > 0) {
            VertexDijkstra node = getMinimum(unSettledNodes);
            settledNodes.add(node);
            unSettledNodes.remove(node);
            findMinimalDistances(node);
        }
    }

    private void findMinimalDistances(VertexDijkstra node) {
        List<VertexDijkstra> adjacentNodes = getNeighbors(node);
        for (VertexDijkstra target : adjacentNodes) {
            if (getShortestDistance(target) > getShortestDistance(node)
                    + getDistance(node, target)) {
                distance.put(target, getShortestDistance(node)
                        + getDistance(node, target));
                predecessors.put(target, node);
                unSettledNodes.add(target);
            }
        }

    }

    private int getDistance(VertexDijkstra node, VertexDijkstra target) {
        for (EdgeDijkstra edge : edges) {
            if (edge.getSource().equals(node)
                    && edge.getDestination().equals(target)) {
                return edge.getWeight();
            }
        }
        throw new RuntimeException("Should not happen");
    }

    private List<VertexDijkstra> getNeighbors(VertexDijkstra node) {
        List<VertexDijkstra> neighbors = new ArrayList<>();
        for (EdgeDijkstra edge : edges) {
            if (edge.getSource().equals(node)
                    && !isSettled(edge.getDestination())) {
                neighbors.add(edge.getDestination());
            }
        }
        return neighbors;
    }

    private VertexDijkstra getMinimum(Set<VertexDijkstra> vertexes) {
        VertexDijkstra minimum = null;
        for (VertexDijkstra vertex : vertexes) {
            if (minimum == null) {
                minimum = vertex;
            } else {
                if (getShortestDistance(vertex) < getShortestDistance(minimum)) {
                    minimum = vertex;
                }
            }
        }
        return minimum;
    }

    private boolean isSettled(VertexDijkstra vertex) {
        return settledNodes.contains(vertex);
    }

    private int getShortestDistance(VertexDijkstra destination) {
        Integer d = distance.get(destination);
        if (d == null) {
            return Integer.MAX_VALUE;
        } else {
            return d;
        }
    }

    /*
     * This method returns the path from the source to the selected target and
     * NULL if no path exists
     */
    public LinkedList<VertexDijkstra> getPath(VertexDijkstra target) {
        LinkedList<VertexDijkstra> path = new LinkedList<>();
        VertexDijkstra step = target;
        // check if a path exists
        if (predecessors.get(step) == null) {
            return null;
        }
        path.add(step);
        while (predecessors.get(step) != null) {
            step = predecessors.get(step);
            path.add(step);
        }
        // Put it into the correct order
        Collections.reverse(path);
        return path;
    }
}
