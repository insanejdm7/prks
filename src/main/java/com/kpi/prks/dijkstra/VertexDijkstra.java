package com.kpi.prks.dijkstra;

/**
 * Created by insane on 16.09.14.
 */
public class VertexDijkstra {
    final private Integer id;
    final private String name;


    public VertexDijkstra(Integer id) {
        this.id = id;
        this.name = id.toString();
    }

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((id == null) ? 0 : id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        VertexDijkstra other = (VertexDijkstra) obj;
        if (id == null) {
            if (other.id != null)
                return false;
        } else if (!id.equals(other.id))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return id.toString();
    }

}
