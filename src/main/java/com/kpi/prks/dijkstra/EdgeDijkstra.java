package com.kpi.prks.dijkstra;

/**
 * Created by insane on 16.09.14.
 */
public class EdgeDijkstra {
    private final String id;
    private final VertexDijkstra source;
    private final VertexDijkstra destination;
    private final int weight;

    public EdgeDijkstra(VertexDijkstra source, VertexDijkstra destination, int weight) {
        this.id = source.getId() + " - " + destination.getId();
        this.source = source;
        this.destination = destination;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }
    public VertexDijkstra getDestination() {
        return destination;
    }

    public VertexDijkstra getSource() {
        return source;
    }
    public int getWeight() {
        return weight;
    }

    @Override
    public String toString() {
        return source + " ->" + destination;
    }
}
