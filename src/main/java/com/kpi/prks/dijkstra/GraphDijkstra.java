package com.kpi.prks.dijkstra;

import java.util.List;

/**
 * Created by insane on 16.09.14.
 */
public class GraphDijkstra {
    private final List<VertexDijkstra> vertexes;
    private final List<EdgeDijkstra> edges;

    public GraphDijkstra(List<VertexDijkstra> vertexes, List<EdgeDijkstra> edges) {
        this.vertexes = vertexes;
        this.edges = edges;
    }

    public List<VertexDijkstra> getVertexes() {
        return vertexes;
    }

    public List<EdgeDijkstra> getEdges() {
        return edges;
    }
}
