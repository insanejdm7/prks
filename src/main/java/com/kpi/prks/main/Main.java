package com.kpi.prks.main;

import com.kpi.prks.build.CircleGrafBuilder;
import com.kpi.prks.build.MeshGrafBuilder;
import com.kpi.prks.build.TreeGrafBuilder;
import com.kpi.prks.data.Graf;
import com.kpi.prks.logic.GrafExecutor;
import com.kpi.prks.util.Pair;
import org.fusesource.jansi.AnsiConsole;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static org.fusesource.jansi.Ansi.Color.GREEN;
import static org.fusesource.jansi.Ansi.Color.RED;
import static org.fusesource.jansi.Ansi.ansi;

/**
 * Created by andriiro on 9/17/2014.
 */
public class Main {

    private static int coreCount;
    private static List<Pair<Integer, Integer>> links = new ArrayList<Pair<Integer, Integer>>();

    public static void main1(String[] args) {
        //lab1
        String originalMatrixStr = null;
        String configStr = null;
        try {
            originalMatrixStr = read("matrix");
            configStr = read("config");
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
            System.exit(1);
        }

        initConfig(configStr);

        final Graf grafOriginal = Graf.valueOf(originalMatrixStr);
        final CircleGrafBuilder builder = new CircleGrafBuilder(grafOriginal);
        final int steps = coreCount / grafOriginal.vertexCount() + 1;

        GrafExecutor executor = new GrafExecutor(grafOriginal);

        System.out.printf("%5s|\t%5s|\t%5s|\t%10s|\t%10s|\t%5s\n", "Count", "S", "D", "Avg D", "T", "C");
        System.out.println("-----------------------------------------------------");
        System.out.printf("%5d|\t%5s|\t%5d|\t%.8f|\t%.8f|\t%5d\n",
                grafOriginal.vertexCount(),
                executor.execute().calcS(),
                executor.calcD(),
                executor.calcAvgD(),
                executor.calcT(),
                executor.calcC());

        for (int index = 2; index <= steps; index++) {
            builder.setLinks(links);
            builder.setTimes(index);
            final Graf currentGraf = builder.build();
            executor = new GrafExecutor(currentGraf);
            System.out.printf("%5d|\t%5s|\t%5d|\t%.8f|\t%.8f|\t%5d\n",
                    currentGraf.vertexCount(),
                    executor.execute().calcS(),
                    executor.calcD(),
                    executor.calcAvgD(),
                    executor.calcT(),
                    executor.calcC());
        }

    }

    public static void main2(String[] args) {
        //lab2
//        System.setProperty("jansi.passthrough", "true");
        System.out.println(System.getProperty("jansi.passthrough"));

        AnsiConsole.systemInstall();

        System.out.println(ansi().eraseScreen().fg(RED).a("Hello").fg(GREEN).a(" World").reset());
        System.out.println(ansi().eraseScreen().render("@|red Hello|@ @|green World|@"));

        AnsiConsole.systemUninstall();

        String originalMatrixStr = null;
        String configStr = null;
        try {
            originalMatrixStr = read("matrix1");
            configStr = read("config1");
        } catch (Exception e) {
            System.out.println("Error " + e.getMessage());
            System.exit(1);
        }

        initConfig(configStr);

        final Graf grafOriginal = Graf.valueOf(originalMatrixStr);
        final TreeGrafBuilder builder = new TreeGrafBuilder(grafOriginal);
        final int steps = coreCount / (grafOriginal.vertexCount() - 2);

        GrafExecutor executor = new GrafExecutor(grafOriginal);

        System.out.printf("%5s|\t%5s|\t%5s|\t%10s|\t%10s|\t%5s\n", "Count", "S", "D", "Avg D", "T", "C");
        System.out.println("-----------------------------------------------------");
        System.out.printf("%5d|\t%5s|\t%5d|\t%.8f|\t%.8f|\t%5d\n",
                grafOriginal.vertexCount(),
                executor.execute().calcS(),
                executor.calcD(),
                executor.calcAvgD(),
                executor.calcT(),
                executor.calcC());

        for (int index = 2; index <= steps; index++) {
            builder.setTimes(index);
            builder.setLinks(links);
            final Graf currentGraf = builder.build();
            executor = new GrafExecutor(currentGraf);
            System.out.printf("%5d|\t%5s|\t%5d|\t%.8f|\t%.8f|\t%5d\n",
                    currentGraf.vertexCount(),
                    executor.execute().calcS(),
                    executor.calcD(),
                    executor.calcAvgD(),
                    executor.calcT(),
                    executor.calcC());
        }

    }

    public static void main(String[] args) {
        //lab3
        final String original = "0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0\n" +
                "1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0\n" +
                "1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0\n" +
                "1,1,1,0,0,0,1,0,0,1,0,0,1,0,0,0\n" +
                "0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0\n" +
                "0,0,0,0,1,0,1,1,0,0,0,0,0,0,0,0\n" +
                "0,0,0,1,1,1,0,1,0,1,0,0,1,0,0,0\n" +
                "0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0\n" +
                "0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0\n" +
                "0,0,0,1,0,0,1,0,1,0,1,1,1,0,0,0\n" +
                "0,0,0,0,0,0,0,0,1,1,0,1,0,0,0,0\n" +
                "0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0\n" +
                "0,0,0,1,0,0,1,0,0,1,0,0,0,1,1,1\n" +
                "0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1\n" +
                "0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1\n" +
                "0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0";
        GrafExecutor executor;
        System.out.printf("%5s|\t%5s|\t%5s|\t%10s|\t%10s|\t%5s\n", "Count", "S", "D", "Avg D", "T", "C");
        System.out.println("-----------------------------------------------------");
        for (int times = 1; ; times++) {
            final MeshGrafBuilder builder = new MeshGrafBuilder(Graf.valueOf(original));
            builder.setTimes(times);
            builder.setHorizontalLinks(Arrays.asList(new Pair<>(5, 2), new Pair<>(7, 0), new Pair<>(13, 10), new Pair<>(15, 8)));
            builder.setVerticalLinks(Arrays.asList(new Pair<>(11, 0), new Pair<>(10, 1), new Pair<>(15, 4), new Pair<>(14, 5)));
            final Graf currentGraf = builder.build();
            executor = new GrafExecutor(currentGraf);
            executor.execute();
            System.out.printf("%5d|\t%5s|\t%5d|\t%.8f|\t%.8f|\t%5d\n",
                    currentGraf.vertexCount(),
                    executor.calcS(),
                    executor.calcD(),
                    executor.calcAvgD(),
                    executor.calcT(),
                    executor.calcC());
            if (currentGraf.vertexCount() > 100) {
                break;
            }
        }
    }

    private static String read(String fileName)
            throws IOException {
        BufferedReader br = null;
        try {
            br = new BufferedReader(new FileReader(fileName));
            final StringJoiner stringJoiner = new StringJoiner("\n");
            br.lines().forEach(line -> stringJoiner.add(line));
            return stringJoiner.toString();
        } finally {
            if (br != null) {
                br.close();
            }
        }
    }

    private static void initConfig(String str) {
        final StringTokenizer tokenizer = new StringTokenizer(str, "\n");
        coreCount = Integer.valueOf(tokenizer.nextToken());
        while (tokenizer.hasMoreTokens()) {
            final String stringPair = tokenizer.nextToken();
            final Pair<Integer, Integer> pair = new Pair<>();
            final int separatorIndex = stringPair.indexOf(":");
            pair.setKey(Integer.valueOf(stringPair.substring(0, separatorIndex)));
            pair.setValue(Integer.valueOf(stringPair.substring(separatorIndex + 1)));
            links.add(pair);
        }
    }
}
