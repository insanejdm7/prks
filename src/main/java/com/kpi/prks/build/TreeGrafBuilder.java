package com.kpi.prks.build;

import com.kpi.prks.data.Graf;
import com.kpi.prks.util.Pair;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by insane on 30.09.14.
 */
public class TreeGrafBuilder extends AbstractGrafBuilder {

    private List<Pair<Integer, Integer>> nextPartLinks;

    public TreeGrafBuilder(Graf original) {
        super(original);
    }

    @Override
    public Graf build() {
        if (times == 1) {
            return original;
        }
        nextPartLinks = new LinkedList<>(links);

        final int width = original.vertexCount() + (times - 1) * (original.vertexCount() - 2);
        final boolean matrix[][] = new boolean[width][width];

        for (int indexY = 0; indexY < original.vertexCount(); indexY++) {
            for (int indexX = 0; indexX < original.vertexCount(); indexX++) {
                System.arraycopy(original.getMatrix()[indexY], 0, matrix[indexY], 0, original.vertexCount());
            }
        }

        int actualVertexCount = original.vertexCount();
        for (int index = 2; index <= times; index++) {
            final Pair<Integer, Integer> link = nextPartLinks.remove(0);
            final int nextZeroVertex = actualVertexCount;

            //central vertex with all
            matrix[nextZeroVertex][link.getKey()] = matrix[link.getKey()][nextZeroVertex] = true;
            matrix[nextZeroVertex][link.getValue()] = matrix[link.getValue()][nextZeroVertex] = true;

            for (int i = 1; i <= original.vertexCount() - 3; i++) {
                matrix[nextZeroVertex][nextZeroVertex + i] = matrix[nextZeroVertex + i][nextZeroVertex] = true;
            }

            matrix[link.getValue()][nextZeroVertex + 1] = matrix[nextZeroVertex + 1][link.getValue()] = true;
            matrix[link.getKey()][nextZeroVertex + (original.vertexCount() - 3)] = matrix[nextZeroVertex + (original.vertexCount() - 3)][link.getKey()] = true;

            for (int i = 1; i <= original.vertexCount() - 4; i++) {
                matrix[nextZeroVertex + i + 1][nextZeroVertex + i] = matrix[nextZeroVertex + i][nextZeroVertex + i + 1] = true;
            }

            actualVertexCount += (original.vertexCount() - 2);

            for (final Pair<Integer, Integer> pair : links) {
                final int keyDiff = original.vertexCount() - pair.getKey();
                final int valueDiff = original.vertexCount() - pair.getValue();
                nextPartLinks.add(new Pair(actualVertexCount - keyDiff, actualVertexCount - valueDiff));
            }

        }
        return new Graf(matrix);
    }
}
