package com.kpi.prks.build;

import com.kpi.prks.data.Graf;
import com.kpi.prks.util.Pair;

import java.util.List;

/**
 * Created by andriiro on 10/7/2014.
 */
public abstract class AbstractGrafBuilder
    implements Builder<Graf> {

    protected final Graf original;
    protected int times;
    protected List<Pair<Integer, Integer>> links;

    public AbstractGrafBuilder(Graf original) {
        this.original = original;
    }

    public void setTimes(int times) {
        if (times < 1) {
            throw new IllegalArgumentException("Times value \"" + times + "\n is wrong");
        }
        this.times = times;
    }

    public void setLinks(List<Pair<Integer, Integer>> links) {
        if (links == null | links.size() == 0) {
            throw new IllegalArgumentException("Wrong links parameter " + links);
        }
        this.links = links;
    }
}
