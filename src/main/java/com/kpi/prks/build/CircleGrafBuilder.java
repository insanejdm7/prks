package com.kpi.prks.build;

import com.kpi.prks.data.Graf;
import com.kpi.prks.util.Pair;

/**
 * Created by insane on 17.09.14.
 */
public class CircleGrafBuilder extends AbstractGrafBuilder {

    public CircleGrafBuilder(Graf original) {
        super(original);
    }

    @Override
    public Graf build() {
        if (times == 1) {
            return original;
        }
        final int width = original.vertexCount()*times;
        final boolean[][] result = new boolean[width][width];

        for (int index = 0; index < original.vertexCount(); index++) {
            System.arraycopy(original.getMatrix()[index],0,result[index],0, original.vertexCount());
        }

        for (int time = 2; time <= times; time++) {
            for (int indexH = 0; indexH < original.vertexCount(); indexH++) {
                System.arraycopy(original.getMatrix()[indexH],0,result[original.vertexCount()*(time-1)+indexH],original.vertexCount()*(time-1),original.vertexCount());
            }
            for (Pair<Integer, Integer> pair : links) {
                result[original.vertexCount()*(time-2) + pair.getKey()][original.vertexCount()*(time-1) + pair.getValue()] = true;
                result[original.vertexCount()*(time-1) + pair.getValue()][original.vertexCount()*(time-2) + pair.getKey()] = true;
            }
        }
        for (Pair<Integer, Integer> pair : links) {
            result[pair.getValue()][original.vertexCount()*(times-1) + pair.getKey()] = true;
            result[original.vertexCount()*(times-1) + pair.getKey()][pair.getValue()] = true;
        }

        return new Graf(result);
    }
}
