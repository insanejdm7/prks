package com.kpi.prks.build;

import com.kpi.prks.data.Graf;
import com.kpi.prks.util.Pair;

import java.util.List;

/**
 * Created by insane on 08.10.14.
 */
public class MeshGrafBuilder extends AbstractGrafBuilder {

    private List<Pair<Integer, Integer>> horizontalLinks;
    private List<Pair<Integer, Integer>> verticalLinks;

    public MeshGrafBuilder(Graf original) {
        super(original);
    }

    @Override
    public Graf build() {
        final boolean[][] matrix = cloneOriginal(getCloneCount());
        linkHorizontal(times, matrix);
        linkVertical(times, matrix);
        return new Graf(matrix);
    }

    private void linkHorizontal(int times, boolean[][] matrix) {
        for (int indexHT = 0; indexHT < times; indexHT++) {
            for (int indexWT = times * indexHT; indexWT < times * (indexHT + 1); indexWT++) {
                int cur = indexWT;
                int next = cur == times * (indexHT + 1) - 1 ? times * indexHT : cur + 1;
                for (Pair<Integer, Integer> link : horizontalLinks) {
                    matrix[cur * original.vertexCount() + link.getKey()][next * original.vertexCount() + link.getValue()] =
                            matrix[next * original.vertexCount() + link.getValue()][cur * original.vertexCount() + link.getKey()] = true;
                }
            }
        }
    }

    private void linkVertical(int times, boolean[][] matrix) {
        for (int indexHT = 0; indexHT < times; indexHT++) {
            for (int indexWT = 0; indexWT < times; indexWT++) {
                int cur = indexWT*times+indexHT;
                int next = indexWT == times-1 ? indexHT : (indexWT+1)*times+indexHT;
                for (Pair<Integer, Integer> link : verticalLinks) {
                    matrix[cur * original.vertexCount() + link.getKey()][next * original.vertexCount() + link.getValue()] =
                            matrix[next * original.vertexCount() + link.getValue()][cur * original.vertexCount() + link.getKey()] = true;
                }
            }
        }
    }

    private int getCloneCount() {
        return times * times;
    }

    private boolean[][] cloneOriginal(int times) {
        final int clonedSize = original.vertexCount() * times;
        final boolean[][] matrix = new boolean[clonedSize][clonedSize];
        for (int index = 0; index < times; index++) {
            final int blockStart = index * original.vertexCount();
            for (int indexH = 0; indexH < original.vertexCount(); indexH++) {
                final int startPos = blockStart + indexH;
                System.arraycopy(original.getMatrix()[indexH], 0, matrix[startPos], blockStart, original.vertexCount());
            }
        }
        return matrix;
    }

    public void setHorizontalLinks(List<Pair<Integer, Integer>> horizontalLinks) {
        this.horizontalLinks = horizontalLinks;
    }

    public void setVerticalLinks(List<Pair<Integer, Integer>> verticalLinks) {
        this.verticalLinks = verticalLinks;
    }
}
