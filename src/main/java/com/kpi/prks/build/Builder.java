package com.kpi.prks.build;

/**
 * Created by insane on 17.09.14.
 */
public interface Builder<T> {

    T build();

}
