package com.kpi.prks.dijkstra;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 16.09.14.
 */
public class DijkstraAlgorithmTest {
    final List<VertexDijkstra> vertexDijkstraList = new ArrayList<>();
    final List<EdgeDijkstra> edgeDijkstraList = new ArrayList<>();

    @Before
    public void clear() {
        vertexDijkstraList.clear();
        edgeDijkstraList.clear();
    }

    @Test
    public void fewTests() {
        for (int index = 0; index < 9; index++) {
            vertexDijkstraList.add(new VertexDijkstra(index));
        }

        addEdge(8,0);
        addEdge(8,1);
        addEdge(8,2);
        addEdge(8,3);
        addEdge(8,4);
        addEdge(8,5);
        addEdge(8,6);
        addEdge(8,7);
        addEdge(0,1);
        addEdge(1,2);
        addEdge(2,3);
        addEdge(3,4);
        addEdge(4,5);
        addEdge(5,6);
        addEdge(6,7);
        addEdge(7,0);

        GraphDijkstra graph = new GraphDijkstra(vertexDijkstraList, edgeDijkstraList);
        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
        dijkstra.execute(vertexDijkstraList.get(0));
        List<VertexDijkstra> path = dijkstra.getPath(vertexDijkstraList.get(3));

        assertNotNull(path);
        assertTrue(path.size() > 0);

        for (VertexDijkstra vertex : path) {
            System.out.println(vertex);
        }
    }

    private void addEdge(int from, int to) {
        edgeDijkstraList.add(new EdgeDijkstra(vertexDijkstraList.get(from), vertexDijkstraList.get(to),1));
        edgeDijkstraList.add(new EdgeDijkstra(vertexDijkstraList.get(to), vertexDijkstraList.get(from),1));
    }

}
