package com.kpi.prks.util;

import com.kpi.prks.data.Graf;
import com.kpi.prks.dijkstra.GraphDijkstra;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by insane on 17.09.14.
 */

/**
 * not full tests!!!
 */
public class GraphDijkstraBuilderTest {

    @Test
    public void graphBuilderTest1() {
        final String str = "0,1,0,0,0,0,0,1,1\n1,0,1,0,0,0,0,0,1\n0,1,0,1,0,0,0,0,1\n0,0,1,0,1,0,0,0,1\n0,0,0,1,0,1,0,0,1\n0,0,0,0,1,0,1,0,1\n0,0,0,0,0,1,0,1,1\n1,0,0,0,0,0,1,0,1\n1,1,1,1,1,1,1,1,0";
        final Graf graf = Graf.valueOf(str);
        final GraphDijkstraBuilder builder = new GraphDijkstraBuilder(graf);
        final GraphDijkstra graphDijkstra = builder.build();
        assertEquals(graphDijkstra.getVertexes().size(), 9);
        assertEquals(graphDijkstra.getEdges().size(), 32);
    }

    @Test
    public void graphBuilderTest2() {
        final String str = "0,1,0,1,1,0,0,0\n1,0,1,0,0,1,0,0\n0,1,0,1,0,0,1,0\n1,0,1,0,0,0,0,1\n1,0,0,0,0,1,0,1\n0,1,0,0,1,0,1,0\n0,0,1,0,0,1,0,1\n0,0,0,1,1,0,1,0";
        final Graf graf = Graf.valueOf(str);
        final GraphDijkstraBuilder builder = new GraphDijkstraBuilder(graf);
        final GraphDijkstra graphDijkstra = builder.build();
        assertEquals(graphDijkstra.getVertexes().size(), 8);
        assertEquals(graphDijkstra.getEdges().size(), 24);
    }

}
