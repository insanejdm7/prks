package com.kpi.prks.data;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by insane on 15.09.14.
 */
public class GrafTest {

    @Test
    public void valueOfStringSuccessfulTest() {
        final String str = "1,0,0,0\n0,1,0,0\n0,0,1,0\n0,0,0,1";
        assertEquals(str, Graf.valueOf(str).toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueOfStringWrongLineLengthTest() {
        final String str = "1,0,0,0,0\n0,1,0,0,0\n0,0,1,0\n0,0,0,1,0\n0,0,0,0,1";
        Graf.valueOf(str);
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueOfStringWrongValue() {
        final String str = "1,0,0,0,0\n0,1,0,0,0\n0,0,1,0\n0,0,0,1,0\n0,0,0,p,1";
        Graf.valueOf(str);
    }

    @Test(expected = IllegalArgumentException.class)
    public void valueOfStringMissedValue() {
        final String str = "1,0,0,0,0\n0,1,0,0,0\n0,0,1,0\n0,0,,1,0\n0,0,0,0,1";
        Graf.valueOf(str);
    }

}
