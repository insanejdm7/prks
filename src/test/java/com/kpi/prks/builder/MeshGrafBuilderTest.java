package com.kpi.prks.builder;

import com.kpi.prks.build.MeshGrafBuilder;
import com.kpi.prks.data.Graf;
import com.kpi.prks.util.Pair;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by insane on 08.10.14.
 */
public class MeshGrafBuilderTest {

    @Test
    public void test() {
        final String original = "0,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0\n" +
                "1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0\n" +
                "1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0\n" +
                "1,1,1,0,0,0,1,0,0,1,0,0,1,0,0,0\n" +
                "0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0\n" +
                "0,0,0,0,1,0,1,1,0,0,0,0,0,0,0,0\n" +
                "0,0,0,1,1,1,0,1,0,1,0,0,1,0,0,0\n" +
                "0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0\n" +
                "0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0\n" +
                "0,0,0,1,0,0,1,0,1,0,1,1,1,0,0,0\n" +
                "0,0,0,0,0,0,0,0,1,1,0,1,0,0,0,0\n" +
                "0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0\n" +
                "0,0,0,1,0,0,1,0,0,1,0,0,0,1,1,1\n" +
                "0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1\n" +
                "0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,1\n" +
                "0,0,0,0,0,0,0,0,0,0,0,0,1,1,1,0";
        final MeshGrafBuilder builder = new MeshGrafBuilder(Graf.valueOf(original));
        builder.setTimes(1);
        builder.setHorizontalLinks(Arrays.asList(new Pair<>(5, 2), new Pair<>(7, 0), new Pair<>(13, 10), new Pair<>(15, 8)));
        builder.setVerticalLinks(Arrays.asList(new Pair<>(11, 0), new Pair<>(10, 1), new Pair<>(15, 4), new Pair<>(14, 5)));
        builder.build();
    }

}
