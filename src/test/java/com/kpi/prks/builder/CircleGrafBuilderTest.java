package com.kpi.prks.builder;

import com.kpi.prks.build.CircleGrafBuilder;
import com.kpi.prks.data.Graf;
import com.kpi.prks.util.Pair;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 17.09.14.
 */
public class CircleGrafBuilderTest {

    @Test
    public void buidTest1() {
        final String originStr = "0,1,0,1\n1,0,1,0\n0,1,0,1\n1,0,1,0";
        final CircleGrafBuilder circleGrafBuilder = new CircleGrafBuilder(Graf.valueOf(originStr));
        final Pair<Integer, Integer> link = new Pair<>();
        link.setKey(1);
        link.setValue(1);
        final List<Pair<Integer, Integer>> links = new ArrayList<Pair<Integer, Integer>>();
        links.add(link);
        circleGrafBuilder.setTimes(3);
        circleGrafBuilder.setLinks(links);
        circleGrafBuilder.build();
    }

    @Test
    public void buildTest2() {
        final String originalStr = "0,1,0,1\n1,0,1,0\n0,1,0,1\n1,0,1,0";
        final String expectedStr = "0,1,0,1,0,0,1,0,0,0,0,0\n"
                + "1,0,1,0,0,0,0,1,0,0,0,0,\n"
                + "0,1,0,1,0,0,0,0,1,0,0,0\n"
                + "1,0,1,0,0,0,0,0,0,1,0,0\n"
                + "0,0,0,0,0,1,0,1,0,0,1,0\n"
                + "0,0,0,0,1,0,1,0,0,0,0,1\n"
                + "1,0,0,0,0,1,0,1,0,0,0,0\n"
                + "0,1,0,0,1,0,1,0,0,0,0,0\n"
                + "0,0,1,0,0,0,0,0,0,1,0,1\n"
                + "0,0,0,1,0,0,0,0,1,0,1,0\n"
                + "0,0,0,0,1,0,0,0,0,1,0,1\n"
                + "0,0,0,0,0,1,0,0,1,0,1,0\n";
        final CircleGrafBuilder circleGrafBuilder = new CircleGrafBuilder(Graf.valueOf(originalStr));
        final Pair<Integer, Integer> link1 = new Pair<>();
        link1.setKey(1);
        link1.setValue(3);
        final Pair<Integer, Integer> link2 = new Pair<>();
        link2.setKey(0);
        link2.setValue(2);
        final List<Pair<Integer, Integer>> links = new ArrayList<Pair<Integer, Integer>>();
        links.add(link1);
        links.add(link2);
        circleGrafBuilder.setTimes(3);
        circleGrafBuilder.setLinks(links);
        assertTrue(Graf.valueOf(expectedStr).equals(circleGrafBuilder.build()));
    }
}
