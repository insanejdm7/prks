package com.kpi.prks.builder;

import com.kpi.prks.build.TreeGrafBuilder;
import com.kpi.prks.data.Graf;
import com.kpi.prks.util.Pair;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.assertTrue;

/**
 * Created by insane on 01.10.14.
 */
public class TreeGrafBuilderTest {

    @Test
    public void simpleTest1() {
        final String original = "0,1,1,1,1,1\n" +
                "1,0,1,0,0,1\n" +
                "1,1,0,1,0,0\n" +
                "1,0,1,0,1,0\n" +
                "1,0,0,1,0,1\n" +
                "1,1,0,0,1,0";
        final String expected = "0,1,1,1,1,1,0,0,0,0\n" +
                "1,0,1,0,0,1,0,0,0,0\n" +
                "1,1,0,1,0,0,0,0,0,0\n" +
                "1,0,1,0,1,0,0,0,0,0\n" +
                "1,0,0,1,0,1,1,1,0,0\n" +
                "1,1,0,0,1,0,1,0,0,1\n" +
                "0,0,0,0,1,1,0,1,1,1\n" +
                "0,0,0,0,1,0,1,0,1,0\n" +
                "0,0,0,0,0,0,1,1,0,1\n" +
                "0,0,0,0,0,1,1,0,1,0";
        final TreeGrafBuilder treeGrafBuilder = new TreeGrafBuilder(Graf.valueOf(original));
        treeGrafBuilder.setLinks(Arrays.asList(new Pair<>(5, 4), new Pair<>(4, 3)));
        treeGrafBuilder.setTimes(2);
        assertTrue(Graf.valueOf(expected).equals(treeGrafBuilder.build()));
    }

    @Test
    public void simpleTest2() {
        final String original = "0,1,1,1,1,1\n" +
                "1,0,1,0,0,1\n" +
                "1,1,0,1,0,0\n" +
                "1,0,1,0,1,0\n" +
                "1,0,0,1,0,1\n" +
                "1,1,0,0,1,0";
        final String expected = "0,1,1,1,1,1,0,0,0,0,0,0,0,0\n" +
                "1,0,1,0,0,1,0,0,0,0,0,0,0,0\n" +
                "1,1,0,1,0,0,0,0,0,0,0,0,0,0\n" +
                "1,0,1,0,1,0,0,0,0,0,1,1,0,0\n" +
                "1,0,0,1,0,1,1,1,0,0,1,0,0,1\n" +
                "1,1,0,0,1,0,1,0,0,1,0,0,0,0\n" +
                "0,0,0,0,1,1,0,1,1,1,0,0,0,0\n" +
                "0,0,0,0,1,0,1,0,1,0,0,0,0,0\n" +
                "0,0,0,0,0,0,1,1,0,1,0,0,0,0\n" +
                "0,0,0,0,0,1,1,0,1,0,0,0,0,0\n" +
                "0,0,0,1,1,0,0,0,0,0,0,1,1,1\n" +
                "0,0,0,1,0,0,0,0,0,0,1,0,1,0\n" +
                "0,0,0,0,0,0,0,0,0,0,1,1,0,1\n" +
                "0,0,0,0,1,0,0,0,0,0,1,0,1,0";
        final TreeGrafBuilder treeGrafBuilder = new TreeGrafBuilder(Graf.valueOf(original));
        treeGrafBuilder.setLinks(Arrays.asList(new Pair<>(5, 4), new Pair<>(4, 3)));
        treeGrafBuilder.setTimes(3);
        assertTrue(Graf.valueOf(expected).equals(treeGrafBuilder.build()));
    }

    @Test
    public void simpleTest3() {
        final String original = "0,1,1,1,1,1\n" +
                "1,0,1,0,0,1\n" +
                "1,1,0,1,0,0\n" +
                "1,0,1,0,1,0\n" +
                "1,0,0,1,0,1\n" +
                "1,1,0,0,1,0";
        final String expected = "0,1,1,1,1,1,0,0,0,0,0,0,0,0,0,0,0,0\n" +
                "1,0,1,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0\n" +
                "1,1,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0\n" +
                "1,0,1,0,1,0,0,0,0,0,1,1,0,0,0,0,0,0\n" +
                "1,0,0,1,0,1,1,1,0,0,1,0,0,1,0,0,0,0\n" +
                "1,1,0,0,1,0,1,0,0,1,0,0,0,0,0,0,0,0\n" +
                "0,0,0,0,1,1,0,1,1,1,0,0,0,0,0,0,0,0\n" +
                "0,0,0,0,1,0,1,0,1,0,0,0,0,0,0,0,0,0\n" +
                "0,0,0,0,0,0,1,1,0,1,0,0,0,0,1,1,0,0\n" +
                "0,0,0,0,0,1,1,0,1,0,0,0,0,0,1,0,0,1\n" +
                "0,0,0,1,1,0,0,0,0,0,0,1,1,1,0,0,0,0\n" +
                "0,0,0,1,0,0,0,0,0,0,1,0,1,0,0,0,0,0\n" +
                "0,0,0,0,0,0,0,0,0,0,1,1,0,1,0,0,0,0\n" +
                "0,0,0,0,1,0,0,0,0,0,1,0,1,0,0,0,0,0\n" +
                "0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,1,1,1\n" +
                "0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,1,0\n" +
                "0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1\n" +
                "0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,1,0";
        final TreeGrafBuilder treeGrafBuilder = new TreeGrafBuilder(Graf.valueOf(original));
        treeGrafBuilder.setLinks(Arrays.asList(new Pair<>(5, 4), new Pair<>(4, 3)));
        treeGrafBuilder.setTimes(4);
        assertTrue(Graf.valueOf(expected).equals(treeGrafBuilder.build()));
    }

}
