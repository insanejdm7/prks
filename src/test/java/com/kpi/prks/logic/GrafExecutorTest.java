package com.kpi.prks.logic;

import com.kpi.prks.data.Graf;
import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

/**
 * Created by insane on 17.09.14.
 */
public class GrafExecutorTest {

    @Test
    public void calcSTest1() {
        final String str = "0,1,0,0,0,0,0,1,1\n1,0,1,0,0,0,0,0,1\n0,1,0,1,0,0,0,0,1\n0,0,1,0,1,0,0,0,1\n0,0,0,1,0,1,0,0,1\n0,0,0,0,1,0,1,0,1\n0,0,0,0,0,1,0,1,1\n1,0,0,0,0,0,1,0,1\n1,1,1,1,1,1,1,1,0";
        assertEquals(8, new GrafExecutor(Graf.valueOf(str)).execute().calcS());
    }

    @Test
    public void calcSTest2() {
        final String str = "0,1,0,1,1,0,0,0\n1,0,1,0,0,1,0,0\n0,1,0,1,0,0,1,0\n1,0,1,0,0,0,0,1\n1,0,0,0,0,1,0,1\n0,1,0,0,1,0,1,0\n0,0,1,0,0,1,0,1\n0,0,0,1,1,0,1,0";
        assertEquals(3, new GrafExecutor(Graf.valueOf(str)).execute().calcS());
    }

    @Test
    public void calcMinMatrixTest1() {
        final String str = "0,1,0,0,0,0,0,1,1\n1,0,1,0,0,0,0,0,1\n0,1,0,1,0,0,0,0,1\n0,0,1,0,1,0,0,0,1\n0,0,0,1,0,1,0,0,1\n0,0,0,0,1,0,1,0,1\n0,0,0,0,0,1,0,1,1\n1,0,0,0,0,0,1,0,1\n1,1,1,1,1,1,1,1,0";
        final int[][] testResult = new GrafExecutor(Graf.valueOf(str)).execute().calcMinMatrix();
        final int[][] rightResult = {{0, 1, 2, 2, 2, 2, 2, 1, 1},
                {1, 0, 1, 2, 2, 2, 2, 2, 1},
                {2, 1, 0, 1, 2, 2, 2, 2, 1},
                {2, 2, 1, 0, 1, 2, 2, 2, 1},
                {2, 2, 2, 1, 0, 1, 2, 2, 1},
                {2, 2, 2, 2, 1, 0, 1, 2, 1},
                {2, 2, 2, 2, 2, 1, 0, 1, 1},
                {1, 2, 2, 2, 2, 2, 1, 0, 1},
                {1, 1, 1, 1, 1, 1, 1, 1, 0}};

        for (int indexH = 0; indexH < testResult.length; indexH++) {
            assertArrayEquals(rightResult[indexH], testResult[indexH]);
        }
    }

    @Test
    public void calcMinMatrixTest2() {
        final String str = "0,1,0,1,1,0,0,0\n1,0,1,0,0,1,0,0\n0,1,0,1,0,0,1,0\n1,0,1,0,0,0,0,1\n1,0,0,0,0,1,0,1\n0,1,0,0,1,0,1,0\n0,0,1,0,0,1,0,1\n0,0,0,1,1,0,1,0";
        final int[][] testResult = new GrafExecutor(Graf.valueOf(str)).execute().calcMinMatrix();
        final int[][] rightResult = {{0, 1, 2, 1, 1, 2, 3, 2},
                {1, 0, 1, 2, 2, 1, 2, 3},
                {2, 1, 0, 1, 3, 2, 1, 2},
                {1, 2, 1, 0, 2, 3, 2, 1},
                {1, 2, 3, 2, 0, 1, 2, 1},
                {2, 1, 2, 3, 1, 0, 1, 2},
                {3, 2, 1, 2, 2, 1, 0, 1},
                {2, 3, 2, 1, 1, 2, 1, 0}};

        for (int indexH = 0; indexH < testResult.length; indexH++) {
            assertArrayEquals(rightResult[indexH], testResult[indexH]);
        }
    }

    @Test
    public void calcDTest1() {
        final String str = "0,1,0,0,0,0,0,1,1\n1,0,1,0,0,0,0,0,1\n0,1,0,1,0,0,0,0,1\n0,0,1,0,1,0,0,0,1\n0,0,0,1,0,1,0,0,1\n0,0,0,0,1,0,1,0,1\n0,0,0,0,0,1,0,1,1\n1,0,0,0,0,0,1,0,1\n1,1,1,1,1,1,1,1,0";
        assertEquals(2, new GrafExecutor(Graf.valueOf(str)).execute().calcD());
    }

    @Test
    public void calcDTest2() {
        final String str = "0,1,0,1,1,0,0,0\n1,0,1,0,0,1,0,0\n0,1,0,1,0,0,1,0\n1,0,1,0,0,0,0,1\n1,0,0,0,0,1,0,1\n0,1,0,0,1,0,1,0\n0,0,1,0,0,1,0,1\n0,0,0,1,1,0,1,0";
        assertEquals(3, new GrafExecutor(Graf.valueOf(str)).execute().calcD());
    }

    @Test
    public void calcAvgDTest1() {
        final String str = "0,1,0,0,0,0,0,1,1\n1,0,1,0,0,0,0,0,1\n0,1,0,1,0,0,0,0,1\n0,0,1,0,1,0,0,0,1\n0,0,0,1,0,1,0,0,1\n0,0,0,0,1,0,1,0,1\n0,0,0,0,0,1,0,1,1\n1,0,0,0,0,0,1,0,1\n1,1,1,1,1,1,1,1,0";
        assertEquals(1.555, new GrafExecutor(Graf.valueOf(str)).execute().calcAvgD(), 0.001);
    }

    @Test
    public void calcAvgDTest2() {
        final String str = "0,1,0,1,1,0,0,0\n1,0,1,0,0,1,0,0\n0,1,0,1,0,0,1,0\n1,0,1,0,0,0,0,1\n1,0,0,0,0,1,0,1\n0,1,0,0,1,0,1,0\n0,0,1,0,0,1,0,1\n0,0,0,1,1,0,1,0";
        assertEquals(1.714, new GrafExecutor(Graf.valueOf(str)).execute().calcAvgD(), 0.001);
    }

    @Test
    public void calcTTest1() {
        final String str = "0,1,0,0,0,0,0,1,1\n1,0,1,0,0,0,0,0,1\n0,1,0,1,0,0,0,0,1\n0,0,1,0,1,0,0,0,1\n0,0,0,1,0,1,0,0,1\n0,0,0,0,1,0,1,0,1\n0,0,0,0,0,1,0,1,1\n1,0,0,0,0,0,1,0,1\n1,1,1,1,1,1,1,1,0";
        assertEquals(0.38875, new GrafExecutor(Graf.valueOf(str)).execute().calcT(), 0.001);
    }

    @Test
    public void calcTTest2() {
        final String str = "0,1,0,1,1,0,0,0\n1,0,1,0,0,1,0,0\n0,1,0,1,0,0,1,0\n1,0,1,0,0,0,0,1\n1,0,0,0,0,1,0,1\n0,1,0,0,1,0,1,0\n0,0,1,0,0,1,0,1\n0,0,0,1,1,0,1,0";
        assertEquals(1.14267, new GrafExecutor(Graf.valueOf(str)).execute().calcT(), 0.001);
    }

    @Test
    public void calcCTest1() {
        final String str = "0,1,0,0,0,0,0,1,1\n1,0,1,0,0,0,0,0,1\n0,1,0,1,0,0,0,0,1\n0,0,1,0,1,0,0,0,1\n0,0,0,1,0,1,0,0,1\n0,0,0,0,1,0,1,0,1\n0,0,0,0,0,1,0,1,1\n1,0,0,0,0,0,1,0,1\n1,1,1,1,1,1,1,1,0";
        assertEquals(144, new GrafExecutor(Graf.valueOf(str)).execute().calcC());
    }

    @Test
    public void calcCTest2() {
        final String str = "0,1,0,1,1,0,0,0\n1,0,1,0,0,1,0,0\n0,1,0,1,0,0,1,0\n1,0,1,0,0,0,0,1\n1,0,0,0,0,1,0,1\n0,1,0,0,1,0,1,0\n0,0,1,0,0,1,0,1\n0,0,0,1,1,0,1,0";
        assertEquals(72, new GrafExecutor(Graf.valueOf(str)).execute().calcC());
    }
}